﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace PalomiesPeli
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {


        public static MainPage MainPagePage;
        private MediaElement mediaElement;

        public MainPage()
        {
            this.InitializeComponent();
            MainPagePage = this;
            //List<Player> players = new List<Player>();
            ApplicationView.PreferredLaunchWindowingMode
            = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.PreferredLaunchViewSize = new Size(1024, 720);
            InitAudio();
          
            
            // mediaElement.Stop();
            mediaElement.Play();
          
                
            
            

        }

        private async void InitAudio()
        {
            mediaElement = new MediaElement();
                mediaElement.AutoPlay = false;
                StorageFolder folder =
                    await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Assets");
                StorageFile file =
                    await folder.GetFileAsync("bensound-dance_01.mp3");
                var stream = await file.OpenAsync(FileAccessMode.Read);
                mediaElement.SetSource(stream, file.ContentType);
            
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            mediaElement.Play();
            this.Frame.Navigate(typeof(GamePage));

        }

        private void HighScoreButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(HighScore));
        }
        /* // butterfly
private Palomies palomies;
// game loop timer
private DispatcherTimer timer;

// lista putoavista obuista
private List<fallingObject> fallingObjects = new List<fallingObject>();

// which keys are pressed
private bool RightPressed;
private bool LeftPressed;


public fallingObject FallingObject;

private DispatcherTimer fallingtimer;

private int Collected = 0;
private int Speed = 5;
private int Step = 2;
private int BirthTimer = 4; 
private int Points;
private int Life=3;


public MainPage()
{
this.InitializeComponent();


ApplicationView.PreferredLaunchWindowingMode
= ApplicationViewWindowingMode.PreferredLaunchViewSize;
ApplicationView.PreferredLaunchViewSize = new Size(1024, 720);

// key listeners
Window.Current.CoreWindow.KeyDown += CoreWindow_KeyDown;
Window.Current.CoreWindow.KeyUp += CoreWindow_KeyUp;

fallingtimer = new DispatcherTimer();
fallingtimer.Tick += fallingTimer_Tick;
fallingtimer.Interval = new TimeSpan(0, 0, 0, BirthTimer);
fallingtimer.Start();



//CreateFallingObject();
CreatePalomies();
StartGame();
}

private void CoreWindow_KeyUp(CoreWindow sender, KeyEventArgs args)
{
switch (args.VirtualKey)
{
case VirtualKey.Left:
   LeftPressed = false;
   break;
case VirtualKey.Right:
   RightPressed = false;
   break;
default:
   break;

}
}

private void CoreWindow_KeyDown(CoreWindow sender, KeyEventArgs args)
{
switch (args.VirtualKey)
{
case VirtualKey.Left:
   LeftPressed = true;
   break;
case VirtualKey.Right:
   RightPressed = true;
   break;
default:
   break;

}
}

private void CreatePalomies()
{
palomies = new Palomies
{
LocationX = MyCanvas.Width / 2 - 75,
LocationY = 530
};
// add to canvas
MyCanvas.Children.Add(palomies);
// show in right location
palomies.SetLocation();
}
private void CreateFallingObject()
{
Random rdnX = new Random();
Random rdnY = new Random();
int StartingPlaceX = rdnX.Next(1, 5) * 100;
int StartingPlaceY = rdnY.Next(0, 2) * 100;

if (Collected % 10 == 0 && Collected > 0)
{
Speed += Step;

}

if (Collected % 5 == 0 && Collected > 0)
{
fallingtimer.Stop();
BirthTimer--;
fallingtimer.Start();
}

FallingObject = new fallingObject
{
LocationX = StartingPlaceX,
LocationY = StartingPlaceY
//LocationX = MyCanvas.Width / 2 - 75,
//LocationY = MyCanvas.Height / 2 - 66
};

FallingObject.dropSpeed = Speed;
// add to canvas
MyCanvas.Children.Add(FallingObject);
// show in right location
fallingObjects.Add(FallingObject);

}


private void fallingTimer_Tick(object sender, object e)
{
CreateFallingObject();
}


private void StartGame()
{
timer = new DispatcherTimer();
timer.Tick += Timer_Tick;
timer.Interval = new TimeSpan(0, 0, 0, 0, 1000 / 60);
timer.Start(); // stop!

}
private void Timer_Tick(object sender, object e)
{
// move palomies
if (LeftPressed) palomies.Move(-1);
if (RightPressed) palomies.Move(1);
// update palomies location
palomies.SetLocation();

CheckCollision();

}

// check collision with flowers and butterfly
private void CheckCollision()
{

foreach (fallingObject FallingObject in fallingObjects) { 
// get rects
Rect r1 = new Rect((palomies.LocationX + 30), (palomies.LocationY + 30), 40, 5); // palomies
Rect r2 = new Rect(FallingObject.LocationX, FallingObject.LocationY, FallingObject.ActualWidth, FallingObject.ActualHeight); // loota
Rect r3 = new Rect(0, 600, 600, 10);


r1.Intersect(r2);
// if not empty
if (!r1.IsEmpty)
{
// remove flower
MyCanvas.Children.Remove(FallingObject);
//CreateFallingObject();
fallingObjects.Remove(FallingObject);
Points++;
Collected++;

   pointsTextBlock.Text = "Pisteet: " + Points;
// end looping...
break;
}



r3.Intersect(r2);
if (!r3.IsEmpty)
{ 
   MyCanvas.Children.Remove(FallingObject);
   fallingObjects.Remove(FallingObject);
   Life--;
   LifeTextBlock.Text = "Elämät: " + Life;
   break;
}


}




if (Life == 0)
{
fallingtimer.Stop();
fallingObjects.Clear();
}
}
}*/
    }
}
