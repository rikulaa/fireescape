﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace PalomiesPeli
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GamePage : Page
    {

        // Palomies
        private Palomies palomies;
        // Pelin aloitustimer
        private DispatcherTimer timer;
        // putoavien objektien timer
        private DispatcherTimer fallingtimer;
        // Taustakuvan animointi
        private DispatcherTimer animate_timer;

        // Putoava objekti sekä verilammikko
        public fallingObject FallingObject;
        public BloodPool bloodPool;

        // lista putoavista obuista
        private List<fallingObject> fallingObjects = new List<fallingObject>();
        private List<BloodPool> bloodPools = new List<BloodPool>();

        // Tutkitaan näppäinten painallusta
        private bool RightPressed;
        private bool LeftPressed;


        

        // taustakuvan frame
        private int currentFrame = 0;

        // Putoavien objektien tippumisnopeus
        private double Speed = 5;

        // Määrä paljonko nopeus kasvaa, step
        private double Step = 0.1;

        // tippuvien olioiden ajastin/aikaväli -muuttuja
        private int BirthTimer = 3000;

        // tutkii kerättyjä pisteitä
        private int Collected = 0;

        // Kerätyt pisteet
        private int Points;

        // Jäljellä olevat elämät
        private int Life = 3;
        
        private MediaElement screamSound;
        private MediaElement splatSound;

        public GamePage()
        {
            this.InitializeComponent();

            // Yritetään asettaa ikkunalle kiinteä koko, missä sovellus suoritetaan
            ApplicationView.PreferredLaunchWindowingMode
            = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.PreferredLaunchViewSize = new Size(1024, 720);

            // Rekisteröidään näppäinpainallukset
            Window.Current.CoreWindow.KeyDown += CoreWindow_KeyDown;
            Window.Current.CoreWindow.KeyUp += CoreWindow_KeyUp;

            // Luodaan tippuville objekteille syntyvyysajastin (Millä aikavälillä syntyy uusi putoava objekti) ja käynnistetään se
            fallingtimer = new DispatcherTimer();
            fallingtimer.Tick += fallingTimer_Tick;
            fallingtimer.Interval = new TimeSpan(0, 0, 0, 0, BirthTimer);
            fallingtimer.Start();

            // Luodaan taustakuvan animointiin tarpeellinen timer ja käynnistetään se
            animate_timer = new DispatcherTimer();
            animate_timer.Tick += Animate_timer_Tick;
            animate_timer.Interval = new TimeSpan(0, 0, 0, 0, 120);
            animate_timer.Start();

            // Alustetaan äänet
            InitScream();
            InitSplat();

            //CreateFallingObject();
            CreatePalomies();
            StartGame();
        }
        /// <summary>
        /// Aloitetaan peli käynnistämällä StartGame ajastin
        /// </summary>
        private void StartGame()
        {
            // 
            timer = new DispatcherTimer();
            timer.Tick += Timer_Tick;
            timer.Interval = new TimeSpan(0, 0, 0, 0, 1000 / 60);
            timer.Start(); 

        }

        private void Animate_timer_Tick(object sender, object e)
        {
            // Vaihdetaan framea 1 ja 0 välillä jolloin taustakuva animoituu
            if (currentFrame == 0)
            {
                currentFrame++;

            }
            else
            {
                currentFrame--;
            }
            SpriteSheetOffset.Y = currentFrame * -720;
        }

        private void CoreWindow_KeyUp(CoreWindow sender, KeyEventArgs args)
        {
            // Lauseke millä kuunnellaan jos näppäin EI OLE painettuna
            switch (args.VirtualKey)
            {
                case VirtualKey.Left:
                    LeftPressed = false;
                    break;
                case VirtualKey.Right:
                    RightPressed = false;
                    break;
                default:
                    break;

            }
        }

        private void CoreWindow_KeyDown(CoreWindow sender, KeyEventArgs args)
        {
            // Lauseke millä kuunnellaan jos näppäin ON painettuna
            switch (args.VirtualKey)
            {
                case VirtualKey.Left:
                    LeftPressed = true;
                    break;
                case VirtualKey.Right:
                    RightPressed = true;
                    break;
                default:
                    break;

            }
        }
        /// <summary>
        /// Luodaan palomies, alustetaan sen sijanti sekä lisätään se canvakselle.
        /// </summary>
        private void CreatePalomies()
        {
            palomies = new Palomies
            {
                LocationX = MyCanvas.Width / 2 - 75,
                LocationY = 600
            };
            // Lisätään canvakselle
            MyCanvas.Children.Add(palomies);
            // Sijoitetaan palomies oikeaan kohtaan
            palomies.SetLocation();
        }
      

        /// <summary>
        /// Ajastin joka luo uuden putoavan objektin määrätyin väliajoin
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fallingTimer_Tick(object sender, object e)
        {
            CreateFallingObject();
        }

        /// <summary>
        /// funktio joka luo putoavia objekteja
        /// </summary>
        private void CreateFallingObject()
        {
            
            // tutkitaan montako pistettä käyttäjä on kerännyt. Sen mukaan vaikeutetaan peliä lisäämällä putoaville objekteille nopeutta, sekä syntyvyyttä
            // joka toisella kerätyllä pisteellä 
            if (Collected % 2 == 0 && Collected > 0)
            {
                // lisätään putoavien objektien nopeutta
                Speed += Step;                
            }

            // jokaisella pisteellä
            if (Collected % 1 == 0 && Collected > 0)
            {
                // nopeutetaan putoavien objektien syntyvyyttä
                fallingtimer.Stop();
                BirthTimer = BirthTimer - 100;

                // syntyvyyden maksinopeus
                if (BirthTimer < 1000)
                {
                    BirthTimer = 1000;
                }

                // käynnistetään timeri uudestaan
                fallingtimer.Interval = new TimeSpan(0, 0, 0, 0, BirthTimer);
                fallingtimer.Start();
            }

            // arvotaan random luvut, jotka määrittävät objektin x, sekä y sijainnin syntyessä.
            Random rdnX = new Random();
            Random rdnY = new Random();

            // kerrotaan 100, jolloin kokonaisluku voidaan suoraan muutta pikseleiksi
            int StartingPlaceX = rdnX.Next(1, 6) * 100;
            int StartingPlaceY = rdnY.Next(0, 2) * 100;

            // luo uuden fallingobjektin
            FallingObject = new fallingObject
            {
                // annettaan objektille x ja y kordinaatit sekä korjataan sijanti niin että objekti tippuu aina ikkunan kohdalta (-30, +10)
                LocationX = StartingPlaceX - 30,
                LocationY = StartingPlaceY + 10
            };

            // annettaan objektille putoamisnopeus
            FallingObject.dropSpeed = Speed;

            // Lisätään objekti canvakselle
            MyCanvas.Children.Add(FallingObject);

            // Lisätään objekti taulukkoon
            fallingObjects.Add(FallingObject);
           
            // Soitetaan huutoääni objektin luomisen yhteydessä
            screamSound.Play();
        }

   /// <summary>
   /// Palomiehen liikkuminen näytöllä,
   /// </summary>
   /// <param name="sender"></param>
   /// <param name="e"></param>
        private void Timer_Tick(object sender, object e)
        {
            // Liikutetaan palomiestä
            if (LeftPressed) palomies.Move(-1);
            if (RightPressed) palomies.Move(1);

            // Päivitetään sijainti canvakselle
            palomies.SetLocation();

            // tutkitaan yhteentörmäystä
            CheckCollision();
        }

        /// <summary>
        /// Tutkitaan yhteentörmäystä palomiehen sekä putoavien objektien kanssa
        /// </summary>
        private void CheckCollision()
        {
            // Tutkitaan taulukon sisällä olevia objekteja ja niiden yhteentörmäyksiä
            foreach (fallingObject FallingObject in fallingObjects)
            {
                // Luodaan nelikulmiot. Yksi palomiehen ympärille, yksi putoavan objektin ympärille ja yksi katutasolle
                // palomies
                Rect r1 = new Rect((palomies.LocationX + 30), (palomies.LocationY + 30), 40, 5); 
                // putoava objekti
                Rect r2 = new Rect(FallingObject.LocationX, FallingObject.LocationY, FallingObject.ActualWidth, FallingObject.ActualHeight); 
                // Katutasolla oleva törmäyskohta
                Rect r3 = new Rect(0, 670, 600, 10);

                // tutkitaan kohtaako palomies ja putoava objekti
                r1.Intersect(r2);
                // jos kohtaavat
                if (!r1.IsEmpty)
                {
                    // Poistetaan putoava objekti canvakselta
                    MyCanvas.Children.Remove(FallingObject);

                    // Poistetaan putoava objekti taulukosta
                    fallingObjects.Remove(FallingObject);

                    // Lisätään piste
                    Points++;
                    Collected++;
                    
                    // näytetään pisteet tekstiboxissa, ruudulla
                    pointsTextBlock.Text = "Pisteet: " + Points;
                    break;
                }
                
                // tutkitaan kohtaako putoava objekti katutason
                r3.Intersect(r2);
                // jos kohtaa
                if (!r3.IsEmpty)
                {
                    // luodaan uusi verilätäkkö
                    bloodPool = new BloodPool
                    {
                        // y kordinaatti on vakio, x kordinaatti vaihtelee putoavan objektin sijainnin mukaan
                        LocationX = r2.X,
                        LocationY = 625
                    };

                    // lisätään verilätäkkö taulukkoon
                    bloodPools.Add(bloodPool);

                    // päivitetään lätäkölle oikea sijainti
                    bloodPool.SetBloodPool();

                    // lisätään lätäkkö canvakselle
                    MyCanvas.Children.Add(bloodPool);

                    //Debug.WriteLine(FallingObject.LocationY);

                    // poistetaan putoava objekti canvakselta
                    MyCanvas.Children.Remove(FallingObject);

                    // poistetaan putoava objekti taulukosta
                    fallingObjects.Remove(FallingObject);

                    // vähennetään elämää
                    Life--;

                    // ilmoitetaan käyttäjälle elämien määrä
                    LifeTextBlock.Text = "Elämät: " + Life;

                    // toistetaan ketsuppiääni
                    splatSound.Play();

                    break;
                }
            }

            // jos elämät loppuvat
            if (Life == 0)
            {
                // pysäytetään ajastimet, poistetaan putoavat objektit taulukosta sekä canvakselta
                fallingtimer.Stop();
                fallingObjects.Clear();
                MyCanvas.Children.Remove(FallingObject);

                // poistetaan verilätäköt canvakselta sekä taulukosta
                bloodPools.Clear();
                MyCanvas.Children.Remove(bloodPool);

                timer.Stop();

                // sovellukset käytössä oleva yhteinen luokka, mihin tallennetaan pisteet jotta päästään niihin käsiksi seuraavalla sivulla
                (App.Current as App).FinalScore = Points;

                // siirryttään tallentamaana pisteet toiselle sivulle
                this.Frame.Navigate(typeof(SaveScore));
            }
        }

        /// <summary>
        /// Alustetaan huutoääni
        /// </summary>
        private async void InitScream()
        {
            // luodaan uusimediaelementti
            screamSound = new MediaElement();
            // ei lähde automaattisesti soimaan
            screamSound.AutoPlay = false;

            // määritetään oikea polku äänitiedostolle
            StorageFolder folder =
                await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Assets");
            StorageFile file =
                await folder.GetFileAsync("scream.mp3");
            var stream = await file.OpenAsync(FileAccessMode.Read);
            screamSound.SetSource(stream, file.ContentType);

        }

        /// <summary>
        /// Alustetaan ketsuppiääni
        /// </summary>
        private async void InitSplat()
        {
            splatSound = new MediaElement();
            splatSound.AutoPlay = false;
            StorageFolder folder =
                await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Assets");
            StorageFile file =
                await folder.GetFileAsync("splat.wav");
            var stream = await file.OpenAsync(FileAccessMode.Read);
            splatSound.SetSource(stream, file.ContentType);

        }
        /// <summary>
        /// Takaisin painikkeen funktio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            // pysäytetään ajastimet, poistetaan putoavat objektit taulukosta sekä canvakselta
            fallingtimer.Stop();
            fallingObjects.Clear();
            MyCanvas.Children.Remove(FallingObject);
            timer.Stop();

            // poistetaan verilätäköt canvakselta sekä taulukosta
            bloodPools.Clear();
            MyCanvas.Children.Remove(bloodPool);

            // määritetään nykyinen sivu
            Frame rootFrame = Window.Current.Content as Frame;
            // tutkitaan onnisutiko
            if (rootFrame == null) return;
            // mennään takaisin etusivulle
            if (rootFrame.CanGoBack)
            {
                rootFrame.GoBack();
            }
        }
        /// <summary>
        /// Funktio mikä suoritetaan, pisteiden tallennus-sivulta tultaessa. Pysäyttää siis pelin sekä lisää .Back arvoksi = false
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // tutkitaan tullaanko SaveScore-sivulta
            if ((App.Current as App).Back == true)
            {
                // pysäytetään, poistetaan jne..
                fallingtimer.Stop();
                fallingObjects.Clear();
                MyCanvas.Children.Remove(FallingObject);
                timer.Stop();

                bloodPools.Clear();
                MyCanvas.Children.Remove(bloodPool);

                (App.Current as App).Back = false;
            }
        }
    }
}

