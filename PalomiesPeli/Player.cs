﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PalomiesPeli
{

    public class Player
    {
        public string name { get; set; }
        public int points { get; set; }
    }
}
