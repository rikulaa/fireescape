﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace PalomiesPeli
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SaveScore : Page
    {
        //private object players;
        List<Player> players = new List<Player>();
        Player player = new Player();
        //private string str;
        public int Points;
        //private Windows.Storage.StorageFile sampleFile;
        public static SaveScore SaveScorePage;
        //private object formatter;

        public object Properties { get; private set; }
        public string playerName;

        public SaveScore()
        {
            this.InitializeComponent();
            //CreateOrOpenFile();
            //ReadFile();
            getPlayers();
            Points = Convert.ToInt32((App.Current as App).FinalScore);
            SaveScorePage = this;
            string str = Convert.ToString((App.Current as App).FinalScore);
            PointsTextBlockSaveScore.Text = "Peli päättyi! Pisteesi: " + str + " Tallenna pisteesi antamalla nimesi.";
            
        }

        private void ShowPlayers()
        {
            ShowPointsTextBlock.Text = "Players:" + Environment.NewLine;
            Debug.WriteLine("Perkele toimi");
            foreach (Player player in players)
            {
                ShowPointsTextBlock.Text += player.name + " " + player.points + Environment.NewLine;
                Debug.WriteLine("näytä pelaajat:");
            }
        }

        private async void ButtonSaveScore_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // open/create a file
                StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
                StorageFile playersFile = await storageFolder.CreateFileAsync("players.dat", CreationCollisionOption.OpenIfExists);

                // save employees to disk
                Stream stream = await playersFile.OpenStreamForWriteAsync();
                DataContractSerializer serializer = new DataContractSerializer(typeof(List<Player>));
                serializer.WriteObject(stream, players);
                await stream.FlushAsync();
                stream.Dispose();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Following exception has happend (writing): " + ex.ToString());
            }

            //this.Frame.Navigate(typeof(MainPage
        }

        private async void getPlayers()
        {
            try
            {
                // find a file
                StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
                Stream stream = await storageFolder.OpenStreamForReadAsync("players.dat");

                // is it empty
                if (stream == null) players = new List<Player>();

                // read data
                DataContractSerializer serializer = new DataContractSerializer(typeof(List<Player>));
                players = (List<Player>)serializer.ReadObject(stream);
                //ShowPlayers();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Following exception has happend (reading): " + ex.ToString());
            }
        }

        private void GiveNameButton_Click(object sender, RoutedEventArgs e)
        {
            //playerName = NameTextBoxSaveScore.Text;
            Player player = new Player();
            player.name = NameTextBoxSaveScore.Text;
            player.points = (App.Current as App).FinalScore;
            players.Add(player);

        }

        private void BackToMainPage_Click(object sender, RoutedEventArgs e)
        {
            (App.Current as App).Back = true;
            //this.Frame.Navigate(typeof(MainPage));
            // get root frame (which show pages)
            Frame rootFrame = Window.Current.Content as Frame;
            // did we get it correctly
            if (rootFrame == null) return;
            // navigate back if possible
            if (rootFrame.CanGoBack)
            { 
                rootFrame.GoBack();
                rootFrame.GoBack();
            }

        }
       
    }
}
