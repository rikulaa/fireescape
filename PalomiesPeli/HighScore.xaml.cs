﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace PalomiesPeli
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HighScore : Page
    {
        private List<Player> players;

        public HighScore()
        {
            this.InitializeComponent();
            ShowPoints();
        }

        private async void ShowPoints()
        {

            try
            {
                // find a file
                StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
                Stream stream = await storageFolder.OpenStreamForReadAsync("players.dat");

                // is it empty
                //if (stream == null) players = new List<Player>();

                // read data
                DataContractSerializer serializer = new DataContractSerializer(typeof(List<Player>));
                players = (List<Player>)serializer.ReadObject(stream);
                ShowPlayers();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Following exception has happend (reading): " + ex.ToString());
            }
            //ReadEmployees();
            /*ShowPointsTextBlock.Text = "Players:" + Environment.NewLine;
            foreach (Player player in players)
            {
                ShowPointsTextBlock.Text += player.name + " " + player.points + Environment.NewLine;
                Debug.WriteLine("asdf");
                Debug.WriteLine(player.points);
            }*/
        }

        private void ShowPlayers()
        {
            //Scores.Text = "Players:" + Environment.NewLine;
            Debug.WriteLine("Perkele toimi");

            players.Reverse();

            foreach (Player player in players)
            {
                
                Scores.Text += player.points + " " + player.name + Environment.NewLine;
                Debug.WriteLine("näytä pelaajat:");
            }
        }

        private void BackButtonHighScore_Click(object sender, RoutedEventArgs e)
        {
            
            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame == null) return;
            if (rootFrame.CanGoBack)
            {
                rootFrame.GoBack();
            }
        }
    }
}

